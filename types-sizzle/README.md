# Installation
> `npm install --save @types/sizzle`

# Summary
This package contains type definitions for sizzle (https://sizzlejs.com).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/sizzle.

### Additional Details
 * Last updated: Tue, 07 Nov 2023 15:11:36 GMT
 * Dependencies: none

# Credits
These definitions were written by [Leonard Thieu](https://github.com/leonard-thieu).
